# Vite React Typescript Tailwindcss
Typescript + Vite + React.js + Tailwindcss

### Dependency 
```
 "dependencies": {
    "autoprefixer": "^10.4.7",
    "postcss": "^8.4.13",
    "react": "^18.0.0",
    "react-dom": "^18.0.0",
    "react-router-dom": "^6.3.0",
    "tailwindcss": "^3.0.24"
  },
  "devDependencies": {
    "@types/react": "^18.0.0",
    "@types/react-dom": "^18.0.0",
    "@types/react-router-dom": "^5.3.3",
    "@vitejs/plugin-react": "^1.3.0",
    "typescript": "^4.6.3",
    "vite": "^2.9.9"
  }
```

### Command 
```
 "scripts": {
    "dev": "vite",
    "build": "tsc && vite build",
    "preview": "vite preview"
  },

```