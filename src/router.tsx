import { BrowserRouter, Routes, Route } from "react-router-dom"
import App from "./App"
import DoSomething from "./DoSomething"
export default function Router() {
    return <>
        <BrowserRouter>
            <Routes>
                <Route path="/" element={<App></App>}></Route>
                <Route path="/dosomething" element={<DoSomething></DoSomething>}></Route>

            </Routes>

        </BrowserRouter>
    </>
}